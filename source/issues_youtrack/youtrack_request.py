import logging
from datetime import date, timedelta
from functools import cached_property
from logging import Logger
from typing import Optional, Union

from requests import Session, PreparedRequest, Request, Response
from requests.exceptions import InvalidURL, InvalidHeader, HTTPError, RequestException, JSONDecodeError

from personal_script.ini_config_parser import IniConfigParser
from personal_script.youtrack_response import ResponseYoutrack

logger: Logger = logging.getLogger(f"root.{__name__}")

__all__ = ["RequestYT", "RequestParams", "RequestParamsNoDeadline", "RequestParamsWeekDeadline"]


class RequestYT:
    """
    Specifies the instance of the API request to the YouTrack.

    Params:
        session --- the HTTP connection session, Session;\n
        params --- the additional headers of the request to accurate the request, tuple[tuple[str, str], ...];\n
        url --- the webhook to send the API request, str;\n
        headers --- the mandatory headers of the request, default: the HEADERS constant;\n
        method --- the HTTP method to use, default: GET;

    Functions:
        _convert_long_date(long: int or None) --- convert the timestamp to date, date;\n
        send_request() --- send the request to the YouTrack and get the response in the JSON format, list[dict];\n
        parse_response() --- handle the response to represent as the ResponseYoutrack objects,\
        list[ResponseYoutrack];
    """

    _request_num: int = 0
    ini_config_parser: IniConfigParser = IniConfigParser()
    _auth_token: str = ini_config_parser.auth_token
    _headers_tuple: tuple[tuple[str, str], ...] = (
        ("Authorization", f"Bearer {_auth_token}"),
        ("Accept", "application/json"),
        ("Content-Type", "application/json"),
    )
    _url: str = "https://youtrack.protei.ru/api/issues"

    def __init__(
            self,
            session: Optional[Session],
            params: tuple[tuple[str, str], ...],
            url: str = _url,
            headers: tuple[tuple[str, str], ...] = _headers_tuple,
            method: str = "get"):
        self._session: Session = session
        self.params: tuple[tuple[str, str], ...] = params
        self.url: str = url
        self.headers: dict[str, str] = dict([(header[0], header[1]) for header in headers])
        self.method: str = method
        self.response: list[dict] = []

    def __repr__(self):
        return f"<{self.__class__.__name__}(params={self.params}, url={self.url}, headers={self.headers}, " \
               f"method={self.method})>"

    def __str__(self):
        return f"Запрос: {self.method.upper()} {self.url}\n{self.params}\n{self.headers}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"RequestYT: params = {self.params}, url = {self.url}, headers = {self.headers}, " \
                   f"method = {self.method}"

    @property
    def session(self) -> Session:
        """
        Gets the HTTP session.

        :return: the HTTP session.
        :rtype: Session
        """
        return self._session

    @session.setter
    def session(self, value):
        """
        Specifies the HTTP session.

        :param value: the HTTP session
        """
        if isinstance(value, Session):
            self._session = value
        else:
            logging.error(f"Значение должно быть типа Session, получено {type(value)}")

    @staticmethod
    def _convert_long_date(long: Optional[int]) -> date:
        """
        Convert the long int value to the datetime.\n
        The considered value is provided up to milliseconds, so it is required to divide by 1000.

        :param int long: the timestamp
        :return: the date associated with the timestamp.
        :rtype: date
        """
        return date.fromtimestamp(long / 1000) if long else date(1, 1, 1)

    def send_request(self):
        """
        Attempts to send the request and get the response from the YouTrack.\n
        Validates any possible connection, request, or handling problems.

        :return: the response to the request in the JSON format.
        :rtype: list[dict] or None
        """
        try:
            prepared_request: PreparedRequest = Request(
                method=self.method, url=self.url, headers=self.headers, params=self.params).prepare()
            logging.debug(f"\nЗапрос #{self.__class__._request_num}")
            logging.debug(f"\nURL:\n{prepared_request.path_url}\nHeaders:\n{self.headers}\nParams:\n{self.params}")
            response: Response = self.session.send(prepared_request)
            logging.debug(f"\nОтвет:\n{response.text}")
            json_response: list[dict] = response.json()
        except InvalidURL as e:
            logging.error(
                f"{e.__class__.__name__}. Incorrect request URL.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except InvalidHeader as e:
            logging.error(
                f"{e.__class__.__name__}. Incorrect headers.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except ConnectionError as e:
            logging.error(f"{e.__class__.__name__}. Connection failed.\n{e.strerror}")
            raise
        except HTTPError as e:
            logging.error(
                f"{e.__class__.__name__}. General HTTP request error.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except RequestException as e:
            logging.error(
                f"{e.__class__.__name__}. Main request error.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except OSError as e:
            logging.error(f"{e.__class__.__name__}. Global error occurred.\n{e.strerror}")
            raise
        except JSONDecodeError as e:
            logging.error(
                f"{e.__class__.__name__}. JSON decoding error occurred.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        else:
            self.__class__._request_num += 1
            self.response = json_response

    def parse_response(self) -> Optional[list[ResponseYoutrack]]:
        """
        Converts the response to the RequestResult objects.

        :return: the RequestResult instances.
        :rtype: list[ResponseYoutrack]
        """
        parsed_items: list[ResponseYoutrack] = []
        self.send_request()
        if self.response is None:
            logging.error("Ответ на запрос пуст")
            return
        for response_item in self.response:
            issue: str = response_item["idReadable"]
            summary: str = response_item["summary"]
            deadline: date = date(1, 1, 1)
            state: str = ""
            priority: str = ""
            item: dict[str, Union[str, int, dict, None]]
            for item in response_item["customFields"]:
                # specify the issue state
                if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField"):
                    state = item["value"]["name"]
                # specify the issue deadline
                elif item["$type"] == "DateIssueCustomField":
                    deadline = self._convert_long_date(item["value"])
                # specify the issue priority
                elif item["$type"] == "SingleEnumIssueCustomField":
                    priority = item["value"]["name"]
                else:
                    continue
            # nullify the improper date or stringify the deadline date
            if deadline == date(1, 1, 1):
                final_deadline = ""
            else:
                final_deadline = deadline.isoformat()
            parsed_items.append(ResponseYoutrack(issue, state, summary, final_deadline, priority))
        return parsed_items


class RequestParams:
    """
    Specifies the params attribute of the request.

    Params:
        login --- the user login, str;

    Properties:
        _params_field --- the "fields" params value, str;\n
        _params_state --- the "State" params value, str;\n
        _params_assignees --- the Assignee_{} query value, str;\n
        _params_kwargs --- the query value, str;\n
        _params_query --- the "query" params value, str;\n
        params --- the params value of the request, tuple[tuple[str, str], ...];\n
    """
    _states_not_completed: str = ",".join(("Active", "Discuss", "New", "Paused", "Review"))
    _assignees_project: tuple[str] = (
        "Исполнители_Doc", "Исполнители_Doc_ST", "Исполнители_Archive", "Исполнители_Archive_ST")
    _params_custom_field_state: str = "State"
    _params_custom_field_deadline: str = "Дедлайн"
    _params_custom_field_priority: str = "Priority"

    def __init__(self, login: str, **kwargs):
        self.login: str = login
        for k, v in kwargs.items():
            setattr(self, k, v)
        self._attrs: list[str] = [k for k in kwargs]

    def __repr__(self):
        kwargs = {key: getattr(self, key) for key in self._attrs}
        return f"<{self.__class__.__name__}(_login={self.login}, kwargs={kwargs})>"

    def __str__(self):
        params: dict[str, str] = {f"{attr}": f"{value}" for attr, value in self.params}
        return f"{self.__class__.__name__}:\nParams: {params}"

    @cached_property
    def _params_field(self) -> str:
        """
        Gets the "field" params value.

        :return: the params attribute value.
        :rtype: str
        """
        return ",".join(
            ("idReadable", "summary",
             "customFields(value,value(name),value(name,_login),projectCustomField(field(name)))")
        )

    @cached_property
    def _params_state(self) -> str:
        """
        Gets the "State" params value.

        :return: the params attribute value.
        :rtype: str
        """
        return f"State: {self._states_not_completed}"

    @cached_property
    def _params_assignees(self) -> str:
        """
        Gets the "Assignees_{}" params value.

        :return: the params attribute value.
        :rtype: str
        """
        assignees: str = " OR ".join([f"{assignee}: {self.login}" for assignee in self._assignees_project])
        return "".join(("(", assignees, ")"))

    @cached_property
    def _params_kwargs(self) -> str:
        """
        Gets the query attribute values.

        :return: the params attribute value.
        :rtype: str
        """
        return " AND ".join([getattr(self, attr) for attr in self._attrs])

    @cached_property
    def _params_query(self) -> str:
        """
        Gets the "query" params value.

        :return: the params attribute value.
        :rtype: str
        """
        return " AND ".join((self._params_state, self._params_assignees, self._params_kwargs))

    @cached_property
    def params(self) -> tuple[tuple[str, str], ...]:
        return (
            ("fields", self._params_field),
            ("query", self._params_query),
            ("customFields", self._params_custom_field_state),
            ("customFields", self._params_custom_field_deadline),
            ("customFields", self._params_custom_field_priority),
        )


class RequestParamsNoDeadline(RequestParams):
    """Specifies the params of the request to get the issues with no deadline assigned."""
    _params_deadline: dict[str, str] = {"_params_deadline": "Дедлайн: Нет"}

    def __init__(self, login: str):
        super().__init__(login, **self._params_deadline)


class RequestParamsWeekDeadline(RequestParams):
    """Specifies the params of the request to get the issues with a deadline in a week or earlier."""
    _today: date = date.today()
    _today_isoformat: str = _today.isoformat()
    _timedelta: timedelta = timedelta(7.0)
    _in_week: date = _today + _timedelta
    _in_week_isoformat: str = _in_week.isoformat()

    _params_deadline: dict[str, str] = {"_params_deadline": f"Дедлайн: {_today_isoformat} .. {_in_week_isoformat}"}

    def __init__(self, login: str):
        super().__init__(login, **self._params_deadline)
