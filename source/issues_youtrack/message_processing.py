import logging
from email.message import EmailMessage
from logging import Logger
from textwrap import dedent
from typing import Sequence, NamedTuple, Optional

from personal_script.youtrack_response import ResponseYoutrack

logging: Logger = logging.getLogger(__name__)


class MessageConverter:
    """
    Specifies the formatter of the Youtrack responses to the email message content.

    Params:
        login --- the user login, str;\n
        soon_deadline_responses --- the issues with the deadline in a week or earlier, Sequence[ResponseYoutrack];\n
        no_deadline_responses --- the issues with no deadline, Sequence[ResponseYoutrack];\n

    Properties:
        _sorted_soon_deadline_messages --- get the string of the issues with the deadline in a week or earlier, str;\n
        _sorted_no_deadline_messages --- get the string of the issues with no deadline, str;\n

    Functions:
        message_text() --- generate the message content,  str;\n
    """

    _message_start: str = dedent("""\
        Краткая информация о текущих задачах на YouTrack, назначенных через поле Исполнители_:""")
    _message_soon_deadline: str = dedent(f"""\
        Задачи с дедлайном через неделю или менее:""")
    _message_no_deadline: str = dedent(f"""\
        Задачи с не указанным дедлайном:""")

    def __init__(
            self,
            login: str, *,
            soon_deadline_responses: Sequence[ResponseYoutrack],
            no_deadline_responses: Sequence[ResponseYoutrack]):
        self.login: str = login
        self.dict_responses_soon_deadline: dict[str, ResponseYoutrack] = dict()
        self.dict_responses_no_deadline: dict[str, ResponseYoutrack] = dict()

        if soon_deadline_responses:
            self.dict_responses_soon_deadline = {response.issue: response for response in soon_deadline_responses}
        if no_deadline_responses:
            self.dict_responses_no_deadline = {response.issue: response for response in no_deadline_responses}

    def __repr__(self):
        _responses: dict[str, ResponseYoutrack] = {
            **self.dict_responses_no_deadline, **self.dict_responses_soon_deadline}
        return f"<{self.__class__.__name__}(login={self.login}, responses={_responses.values()})>"

    def __str__(self):
        return f"Запросы с Youtrack:\n{self._sorted_soon_deadline_messages}\n{self._sorted_no_deadline_messages}"

    @property
    def _sorted_soon_deadline_messages(self) -> Optional[str]:
        """
        Gets the string of the issues with the deadline in a week or earlier.

        :return: the issues in the message.
        :rtype: str or None
        """
        if self.dict_responses_soon_deadline:
            return "\n".join(str(response) for response in sorted(self.dict_responses_soon_deadline.values()))
        return

    @property
    def _sorted_no_deadline_messages(self) -> Optional[str]:
        """
        Gets the string of the issues with no deadline.

        :return: the issues in the message.
        :rtype: str or None
        """
        if self.dict_responses_no_deadline:
            return "\n".join(str(response) for response in sorted(self.dict_responses_no_deadline.values()))
        return

    def message_text(self) -> Optional[str]:
        """
        Generates the email message content.

        :return: the message text.
        :rtype: str or None
        """
        if not self.dict_responses_soon_deadline and not self.dict_responses_no_deadline:
            logging.debug(f"Назначенных задач нет")
            return
        _soon_deadline_message: str
        _no_deadline_message: str

        if self.dict_responses_soon_deadline:
            _soon_deadline_message = "\n".join((self._message_soon_deadline, self._sorted_soon_deadline_messages))
        else:
            _soon_deadline_message = "Задач с дедлайном через неделю или менее нет."

        if self.dict_responses_no_deadline:
            _no_deadline_message: str = "\n".join((self._message_no_deadline, self._sorted_no_deadline_messages))
        else:
            _no_deadline_message = "Задач с не указанным дедлайном нет."

        _message_structure: tuple[str, ...] = (
            self._message_start,
            "--------------------",
            _soon_deadline_message,
            _no_deadline_message)
        _message_content: str = "\n".join(_message_structure)
        logging.info(f"\n{_soon_deadline_message}\n{_no_deadline_message}")
        return _message_content


class MessageAttributes(NamedTuple):
    """
    Specifies the email message headers.

    "From", "Subject", "Content-Type", "Content-Transfer-Encoding", "Content-Language", "MIME-Version"
    """
    from_: str = None
    subject_: str = None
    content_type_: str = None
    content_transfer_encoding_: str = None
    content_language_: str = None
    mime_version: str = None

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def __str__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def prepare_values(self) -> dict[str, str]:
        """
        Specifies the attributes as a dict.

        :return: the attributes dictionary.
        :rtype: dict[str, str]
        """
        return {k: v for k, v in self._asdict().items() if v is not None}


class MessageYoutrack:
    """
    Specifies the message with the issues.

    Params:
        sender_email --- the email to send the message from, str;\n
        recipient_email --- the email to receive the message, str;\n
        message_text --- the email content, str;\n
        message_kwargs --- the message attributes;\n

    Properties:
        message --- the message to send, bytes;\n
        readable --- the human-readable format of the message, str;\n
    """
    _keys: tuple[str] = (
        "From", "Subject", "Content-Type", "Content-Transfer-Encoding", "Content-Language", "MIME-Version")

    def __init__(self, sender_email: str, recipient_email: str, message_text: str, message_kwargs: MessageAttributes):
        self.sender_email: str = sender_email
        self.recipient_email: str = recipient_email
        self.message_text: str = message_text
        self.message_kwargs: dict[str, str] = message_kwargs.prepare_values()
        self.email_message: bytes = b""
        self.message()

    def __str__(self):
        return f"Сообщение:\n{self.readable}"

    def __repr__(self):
        return f"<{self.__class__.__name__}(sender_email={self.sender_email}, recipient_email={self.recipient_email}," \
               f"message_text={self.message_text}, message_kwargs={self.message_kwargs.keys()})>"

    def message(self):
        """Specifies the email message to send."""
        if not self.message_text:
            return
        message: EmailMessage = EmailMessage()
        message.set_payload(self.message_text)
        for k, v in zip(self._keys, tuple(self.message_kwargs.values())):
            message[k] = v
        message["To"] = self.recipient_email
        self.email_message = message.as_string().encode("utf-8")
        logging.debug(f"Сообщение:\n{self.readable}")

    @property
    def readable(self) -> Optional[str]:
        """
        Specifies the human-readable message formatting.

        :return: the email message text.
        :rtype: str or None
        """
        if self.email_message:
            return self.email_message.decode("utf-8")
        return
