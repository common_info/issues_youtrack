import logging
from logging import Logger, Formatter, StreamHandler, FileHandler
import os
import sys
from pathlib import Path
from shutil import rmtree
from datetime import date, timedelta
from email.message import EmailMessage
from functools import cached_property
from smtplib import SMTP, SMTPConnectError, SMTPServerDisconnected, SMTPNotSupportedError, SMTPAuthenticationError, \
    SMTPResponseException, SMTPException, SMTPHeloError, SMTPSenderRefused, SMTPDataError, SMTPRecipientsRefused
from ssl import create_default_context, SSLContext
from textwrap import dedent
from typing import NamedTuple, Optional, Union, Sequence, Literal, TextIO
from requests import Request, PreparedRequest, Response, HTTPError, RequestException, JSONDecodeError, Session
from requests.exceptions import InvalidURL, InvalidHeader


logger: Logger = logging.getLogger(f"root.{__name__}")


__all__ = ["ResponseYoutrack", "RequestYT", "RequestParams", "RequestParamsNoDeadline", "RequestParamsWeekDeadline",
           "MessageConverter", "MessageAttributes", "MessageYoutrack", "MailServer", "User"]

_headers_tuple: tuple[tuple[str, str], ...] = (
    ("Authorization", " ".join(("Bearer", _auth_token))),
    ("Accept", "application/json"),
    ("Content-Type", "application/json"),
)


class ResponseYoutrack(NamedTuple):
    """
    Contains the parsed response instance.

    Params:
        issue --- the issue name (idReadable), str;\n
        state --- the issue state (customFields.State), str;\n
        summary --- the short issue description (summary), str;\n
        deadline --- the last day to complete the work (customFields.Дедлайн), str (ISO format);\n
        priority --- the issue importance (customFields.Priority), str (Low/Basic/Critical).\n

    Properties:
        hyperlink --- the YouTrack link to the issue, str.

    Functions:
        convert_for_json() --- format the instance to write to the JSON file, dict[str, str].
    """

    issue: str
    state: str
    summary: str
    deadline: str
    priority: str

    def __repr__(self):
        return f"<ResponseYoutrack(issue={self.issue}, state={self.state}, summary={self.summary}," \
               f"deadline={self.deadline}, priority={self.priority})>"

    def __str__(self):
        if self.deadline:
            deadline_string = f"{self.deadline}, до дедлайна: {self.days_before} дней"
        else:
            deadline_string = "не задан"
        return f"Задача YouTrack: {self.issue}, статус: {self.state}, дедлайн: {deadline_string}." \
               f"\nПерейти к задаче: {self.hyperlink}"

    def __hash__(self):
        return hash(self.issue)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.issue == other.issue
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.issue != other.issue
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__) and self.issue != other.issue:
            if self._priority_conversion[self.priority] < other._priority_conversion[other.priority]:
                return True
            elif self._priority_conversion[self.priority] > other._priority_conversion[other.priority]:
                return False
            else:
                if self._deadline_date < other._deadline_date:
                    return True
                elif self._deadline_date > other._deadline_date:
                    return False
                else:
                    return self._issue_number < other._issue_number
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__) and self.issue != other.issue:
            if self.priority_int > other.priority_int:
                return True
            elif self.priority_int < other.priority_int:
                return False
            else:
                if self._deadline_date > other._deadline_date:
                    return True
                elif self._deadline_date < other._deadline_date:
                    return False
                else:
                    return self._issue_number > other._issue_number
        else:
            return NotImplemented

    @property
    def _priority_conversion(self) -> dict[str, int]:
        """
        Mapping of the priorities and integers.

        :return: the dictionary.
        :rtype: dict[str, int]
        """
        return {
            "Critical": 30,
            "Important": 20,
            "Basic": 10,
            "Low": 0
        }

    @property
    def hyperlink(self) -> str:
        """
        Specifies the hyperlink to the issue in the Youtrack.

        :return: the active hyperlink.
        :rtype: str
        """
        return f"https://youtrack.protei.ru/issue/{self.issue}"

    @property
    def days_before(self) -> Optional[int]:
        """
        Specifies the number of remaining days up to the deadline if the deadline exists.

        :return: the number of days or None.
        :rtype: int or None
        """
        if self.deadline:
            _deadline_date: date = date.fromisoformat(self.deadline)
            _today: date = date.today()
            _delta: timedelta = _deadline_date - _today
            return _delta.days
        return None

    @property
    def _deadline_date(self) -> date:
        """
        Converts the deadline to the date format. If the deadline is missing, returns the date in the farthest future.\n
        Used for sorting issues.

        :return: the deadline date.
        :rtype: date
        """
        if self.deadline:
            return date.fromisoformat(self.deadline)
        else:
            return date(9999, 1, 1)

    @property
    def _issue_number(self) -> int:
        """
        Gets the issue number from the full issue id value, <project>-<issue_number>.\n
        Used for sorting issues.

        :return: the issue number.
        :rtype: int
        """
        return int(self.issue.split("-")[1])

    @property
    def priority_int(self) -> int:
        """
        Maps the issue priority to the integer following the conversion table.\n
        Used for sorting issues.

        :return: the associated priority integer value.
        :rtype: int
        """
        return self._priority_conversion[self.priority]


class RequestYT:
    """
    Specifies the instance of the API request to the YouTrack.

    Params:
        url --- the webhook to send the API request, str;\n
        params --- the additional headers of the request to accurate the request, tuple[tuple[str, str], ...];\n
        headers --- the mandatory headers of the request, default: the HEADERS constant;\n
        method --- the HTTP method to use, default: GET.

    Functions:
         send_request() --- send the request to the YouTrack and get the response in the JSON format,
         list[dict] or None;\n
         parse_response() --- handle the response to represent as the ResponseYoutrack objects,
         list[ResponseYoutrack].
    """

    def __init__(
            self,
            session: Optional[Session],
            params: tuple[tuple[str, str], ...],
            url: str = _url,
            headers: tuple[tuple[str, str], ...] = _headers_tuple,
            method: str = "get"):
        self._session: Session = session
        self.params: tuple[tuple[str, str], ...] = params
        self.url: str = url
        self.headers: dict[str, str] = dict([(header[0], header[1]) for header in headers])
        self.method: str = method

    def __repr__(self):
        return f"<{self.__class__.__name__}(params={self.params}, url={self.url}, headers={self.headers}, " \
               f"method={self.method})>"

    def __str__(self):
        return f"Запрос: {self.method.upper()} {self.url}\n{self.params}\n{self.headers}"

    def __format__(self, format_spec):
        if format_spec == "output":
            return f"RequestYT: params = {self.params}, url = {self.url}, headers = {self.headers}, " \
                   f"method = {self.method}"

    @property
    def session(self):
        return self._session

    @session.setter
    def session(self, value):
        self._session = value

    @staticmethod
    def _convert_long_date(long: Optional[int]) -> date:
        """
        Convert the long int value to the datetime.\n
        The considered value is provided up to milliseconds, so it is required to divide by 1000.

        :param int long: the timestamp
        :return: the date associated with the timestamp.
        :rtype: date
        """
        return date.fromtimestamp(long / 1000) if long else date(1, 1, 1)

    def send_request(self) -> Optional[list[dict]]:
        """
        Attempts to send the request and get the response from the YouTrack.\n
        Validates any possible connection, request, or handling problems.

        :return: the response to the request in the JSON format.
        :rtype: list[dict] or None
        """
        try:
            prepared_request: PreparedRequest = Request(
                method=self.method, url=self.url, headers=self.headers, params=self.params).prepare()
            logging.debug(f"Request path url:\n{prepared_request.path_url}")
            response: Response = self.session.send(prepared_request)
            json_response: list[dict] = response.json()
        except InvalidURL as e:
            logging.error(
                f"{e.__class__.__name__}. Incorrect request URL.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except InvalidHeader as e:
            logging.error(
                f"{e.__class__.__name__}. Incorrect headers.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except ConnectionError as e:
            logging.error(f"{e.__class__.__name__}. Connection failed.\n{e.strerror}")
            raise
        except HTTPError as e:
            logging.error(
                f"{e.__class__.__name__}. General HTTP request error.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except RequestException as e:
            logging.error(
                f"{e.__class__.__name__}. Main request error.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        except OSError as e:
            logging.error(f"{e.__class__.__name__}. Global error occurred.\n{e.strerror}")
            raise
        except JSONDecodeError as e:
            logging.error(
                f"{e.__class__.__name__}. JSON decoding error occurred.\n{e.strerror}\n"
                f"Request: {e.request}\nResponse: {e.response}")
            raise
        else:
            logging.debug(f"Response:\n{json_response}")
            return json_response

    def parse_response(self) -> Optional[list[ResponseYoutrack]]:
        """
        Converts the response to the RequestResult objects.

        :return: the RequestResult instances.
        :rtype: list[ResponseYoutrack]
        """
        parsed_items: list[ResponseYoutrack] = []
        if self.send_request() is None:
            logging.error("Ответ на запрос пуст")
            return
        for response_item in self.send_request():
            issue: str = response_item["idReadable"]
            summary: str = response_item["summary"]
            deadline: date = date(1, 1, 1)
            state: str = ""
            priority: str = ""
            item: dict[str, Union[str, int, dict, None]]
            for item in response_item["customFields"]:
                # specify the issue state
                if item["$type"] in ("StateIssueCustomField", "StateMachineIssueCustomField"):
                    state = item["value"]["name"]
                # specify the issue deadline
                elif item["$type"] == "DateIssueCustomField":
                    deadline = self._convert_long_date(item["value"])
                # specify the issue priority
                elif item["$type"] == "SingleEnumIssueCustomField":
                    priority = item["value"]["name"]
                else:
                    continue
            # nullify the improper date or stringify the deadline date
            if deadline == date(1, 1, 1):
                final_deadline = ""
            else:
                final_deadline = deadline.isoformat()
            parsed_items.append(ResponseYoutrack(issue, state, summary, final_deadline, priority))
        return parsed_items


class RequestParams:
    """
    Specifies the params attribute of the request.

    Params:
        login --- the user login, str;

    Properties:
        _params_field --- the "fields" params value, str;\n
        _params_state --- the "State" params value, str;\n
        _params_assignees --- the Assignee_{} query value, str;\n
        _params_kwargs --- the query value, str;\n
        _params_query --- the "query" params value, str;\n
        params --- the params value of the request, tuple[tuple[str, str], ...];\n
    """
    _states_not_completed: str = ",".join(("Active", "Discuss", "New", "Paused", "Review"))
    _assignees_project: tuple[str] = (
        "Исполнители_Doc", "Исполнители_Doc_ST", "Исполнители_Archive", "Исполнители_Archive_ST")
    _params_custom_field_state: str = "State"
    _params_custom_field_deadline: str = "Дедлайн"
    _params_custom_field_priority: str = "Priority"

    def __init__(self, login: str, **kwargs):
        self.login: str = login
        for k, v in kwargs.items():
            setattr(self, k, v)
        self._attrs: list[str] = [k for k in kwargs]

    def __repr__(self):
        kwargs = {key: getattr(self, key) for key in self._attrs}
        return f"<{self.__class__.__name__}(_login={self.login}, kwargs={kwargs})>"

    def __str__(self):
        params: dict[str, str] = {f"{attr}": f"{value}" for attr, value in self.params}
        return f"{self.__class__.__name__}:\nParams: {params}"

    @cached_property
    def _params_field(self) -> str:
        """
        Gets the "field" params value.

        :return: the params attribute value.
        :rtype: str
        """
        return ",".join(
            ("idReadable", "summary",
             "customFields(value,value(name),value(name,_login),projectCustomField(field(name)))")
        )

    @cached_property
    def _params_state(self) -> str:
        """
        Gets the "State" params value.

        :return: the params attribute value.
        :rtype: str
        """
        return f"State: {self._states_not_completed}"

    @cached_property
    def _params_assignees(self) -> str:
        """
        Gets the "Assignees_{}" params value.

        :return: the params attribute value.
        :rtype: str
        """
        assignees: str = " OR ".join([f"{assignee}: {self.login}" for assignee in self._assignees_project])
        return "".join(("(", assignees, ")"))

    @cached_property
    def _params_kwargs(self) -> str:
        """
        Gets the query attribute values.

        :return: the params attribute value.
        :rtype: str
        """
        return " AND ".join([getattr(self, attr) for attr in self._attrs])

    @cached_property
    def _params_query(self) -> str:
        """
        Gets the "query" params value.

        :return: the params attribute value.
        :rtype: str
        """
        return " AND ".join((self._params_state, self._params_assignees, self._params_kwargs))

    @cached_property
    def params(self) -> tuple[tuple[str, str], ...]:
        return (
            ("fields", self._params_field),
            ("query", self._params_query),
            ("customFields", self._params_custom_field_state),
            ("customFields", self._params_custom_field_deadline),
            ("customFields", self._params_custom_field_priority),
        )


class RequestParamsNoDeadline(RequestParams):
    """Specifies the params of the request to get the issues with no deadline assigned."""
    _params_deadline: dict[str, str] = {"_params_deadline": "Дедлайн: Нет"}

    def __init__(self, login: str):
        super().__init__(login, **self._params_deadline)


class RequestParamsWeekDeadline(RequestParams):
    """Specifies the params of the request to get the issues with a deadline in a week or earlier."""
    _today: date = date.today()
    _today_isoformat: str = _today.isoformat()
    _timedelta: timedelta = timedelta(7.0)
    _in_week: date = _today + _timedelta
    _in_week_isoformat: str = _in_week.isoformat()

    _params_deadline: dict[str, str] = {"_params_deadline": f"Дедлайн: {_today_isoformat} .. {_in_week_isoformat}"}

    def __init__(self, login: str):
        super().__init__(login, **self._params_deadline)


class MessageConverter:
    """
    Specifies the formatter of the Youtrack responses to the email message content.

    Params:
        login --- the user login, str;\n
        soon_deadline_responses --- the issues with the deadline in a week or earlier, Sequence[ResponseYoutrack];\n
        no_deadline_responses --- the issues with no deadline, Sequence[ResponseYoutrack];\n

    Properties:
        _sorted_soon_deadline_messages --- get the string of the issues with the deadline in a week or earlier, str;\n
        _sorted_no_deadline_messages --- get the string of the issues with no deadline, str;\n

    Functions:
        message_text() --- generate the message content,  str;\n
        """

    _message_start: str = dedent("""\
            Краткая информация о текущих задачах на YouTrack, назначенных через поле Исполнители_:""")
    _message_soon_deadline: str = dedent(f"""\
            Задачи с дедлайном через неделю или менее:""")
    _message_no_deadline: str = dedent(f"""\
            Задачи с не указанным дедлайном:""")

    def __init__(
            self,
            login: str, *,
            soon_deadline_responses: Sequence[ResponseYoutrack],
            no_deadline_responses: Sequence[ResponseYoutrack]):
        self.login: str = login
        self.dict_responses_soon_deadline: dict[str, ResponseYoutrack] = dict()
        self.dict_responses_no_deadline: dict[str, ResponseYoutrack] = dict()

        if soon_deadline_responses:
            self.dict_responses_soon_deadline = {response.issue: response for response in soon_deadline_responses}
        if no_deadline_responses:
            self.dict_responses_no_deadline = {response.issue: response for response in no_deadline_responses}

    def __repr__(self):
        _responses: dict[str, ResponseYoutrack] = {
            **self.dict_responses_no_deadline, **self.dict_responses_soon_deadline}
        return f"<{self.__class__.__name__}(login={self.login}, responses={_responses.values()})>"

    def __str__(self):
        return f"Запросы с Youtrack:\n{self._sorted_soon_deadline_messages}\n{self._sorted_no_deadline_messages}"

    @property
    def _sorted_soon_deadline_messages(self) -> Optional[str]:
        """
        Gets the string of the issues with the deadline in a week or earlier.

        :return: the issues in the message.
        :rtype: str or None
        """
        if self.dict_responses_soon_deadline:
            return "\n".join(str(response) for response in sorted(self.dict_responses_soon_deadline.values()))
        return

    @property
    def _sorted_no_deadline_messages(self) -> Optional[str]:
        """
        Gets the string of the issues with no deadline.

        :return: the issues in the message.
        :rtype: str or None
        """
        if self.dict_responses_no_deadline:
            return "\n".join(str(response) for response in sorted(self.dict_responses_no_deadline.values()))
        return

    def message_text(self) -> Optional[str]:
        """
        Generates the email message content.

        :return: the message text.
        :rtype: str or None
        """
        if not self.dict_responses_soon_deadline and not self.dict_responses_no_deadline:
            logging.debug(f"Назначенных задач нет")
            return
        _soon_deadline_message: str
        _no_deadline_message: str

        if self.dict_responses_soon_deadline:
            _soon_deadline_message = "\n".join((self._message_soon_deadline, self._sorted_soon_deadline_messages))
        else:
            _soon_deadline_message = "Задач с дедлайном через неделю или менее нет."

        if self.dict_responses_no_deadline:
            _no_deadline_message: str = "\n".join((self._message_no_deadline, self._sorted_no_deadline_messages))
        else:
            _no_deadline_message = "Задач с не указанным дедлайном нет."

        _message_structure: tuple[str, ...] = (
            self._message_start,
            "--------------------",
            _soon_deadline_message,
            _no_deadline_message)
        _message_content: str = "\n".join(_message_structure)
        logging.info(f"\n{_soon_deadline_message}\n{_no_deadline_message}")
        return _message_content


class MessageAttributes(NamedTuple):
    """
    Specifies the email message headers.

    "From", "Subject", "Content-Type", "Content-Transfer-Encoding", "Content-Language", "MIME-Version"
    """
    from_: str = None
    subject_: str = None
    content_type_: str = None
    content_transfer_encoding_: str = None
    content_language_: str = None
    mime_version: str = None

    def __repr__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def __str__(self):
        return f"<{self.__class__.__name__}({self._asdict()})>"

    def prepare_values(self) -> dict[str, str]:
        """
        Specifies the attributes as a dict.

        :return: the attributes dictionary.
        :rtype: dict[str, str]
        """
        return {k: v for k, v in self._asdict().items() if v is not None}


class MessageYoutrack:
    """
    Specifies the message with the issues.

    Params:
        sender_email --- the email to send the message from, str;\n
        recipient_email --- the email to receive the message, str;\n
        message_text --- the email content, str;\n
        message_kwargs --- the message attributes;\n

    Properties:
        message --- the message to send, bytes;\n
        readable --- the human-readable format of the message, str;\n
    """
    _keys: tuple[str] = (
        "From", "Subject", "Content-Type", "Content-Transfer-Encoding", "Content-Language", "MIME-Version")

    def __init__(self, sender_email: str, recipient_email: str, message_text: str, message_kwargs: MessageAttributes):
        self.sender_email: str = sender_email
        self.recipient_email: str = recipient_email
        self.message_text: str = message_text
        self.message_kwargs: dict[str, str] = message_kwargs.prepare_values()
        self.email_message: bytes = b""
        self.message()

    def __str__(self):
        return f"Сообщение:\n{self.readable}"

    def __repr__(self):
        return f"<{self.__class__.__name__}(sender_email={self.sender_email}, recipient_email={self.recipient_email}," \
               f"message_text={self.message_text}, message_kwargs={self.message_kwargs.keys()})>"

    def message(self):
        """Specifies the email message to send."""
        if not self.message_text:
            return
        message: EmailMessage = EmailMessage()
        message.set_payload(self.message_text)
        for k, v in zip(self._keys, tuple(self.message_kwargs.values())):
            message[k] = v
        message["To"] = self.recipient_email
        self.email_message = message.as_string().encode("utf-8")
        logging.debug(f"Сообщение:\n{self.readable}")

    @property
    def readable(self) -> Optional[str]:
        """
        Specifies the human-readable message formatting.

        :return: the email message text.
        :rtype: str or None
        """
        if self.email_message:
            return self.email_message.decode("utf-8")
        return


class MailServer:
    """
    Specifies the mail SMTP server.

    Params:
        host --- the SMTP server DNS name, str;\n
        port --- the SMTP server port, int;\n
        user --- the login to authenticate in the server, str;\n
        password --- the password to authenticate in the server, str;\n

    Functions:
        server() --- run the SMTP server;\n
        send_email(message: MessageYoutrack) --- send the message to the mailbox.
    """

    def __init__(self, host: str = None, port: int = None, user: str = None, password: str = None):
        if host is None:
            host = self._smtp_host
        if port is None:
            port = self._smtp_port
        if user is None:
            user = self._smtp_user
        if password is None:
            password = self._smtp_password

        self.host: str = host
        self.port: int = port
        self.user: str = user
        self.password: str = password
        self.smtp_server: Optional[SMTP] = None

    def __repr__(self):
        return f"<{self.__class__.__name__}(host={self.host}, port={self.port}, user={self.user}, " \
               f"password={self.password})>"

    def __str__(self):
        return f"Mail server: {self.host}:{self.port}, SMTP server active: {self.smtp_server is None}"

    def server(self):
        """Runs the SMTP server to send emails. Further the server is used as a content manager."""
        context: SSLContext = create_default_context()
        smtp_server: SMTP = SMTP(self.host, self.port)
        try:
            logging.info(f"Попытка подключения к SMTP-серверу: {self.host}:{self.port}")
            smtp_server.connect(self.host, self.port)
            # set the connection
            smtp_server.ehlo()
            smtp_server.starttls(context=context)
            smtp_server.ehlo()
            # authenticate
            smtp_server.login(self.user, self.password)
        except SMTPConnectError as e:
            logging.critical(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
            raise
        except SMTPServerDisconnected as e:
            logging.critical(f"{e.__class__.__name__}\nFile: {e.filename}\n{e.strerror}")
            raise
        except SMTPNotSupportedError as e:
            logging.critical(f"{e.__class__.__name__}\nFile: {e.filename}\n{e.strerror}")
            raise
        except SMTPAuthenticationError as e:
            logging.critical(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
            raise
        except SMTPResponseException as e:
            logging.critical(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
            raise
        except SMTPException as e:
            logging.critical(f"{e.__class__.__name__}\nFile: {e.filename}\n{e.strerror}")
            raise
        except ConnectionRefusedError as e:
            logging.critical(f"{e.__class__.__name__}\nFile: {e.filename}\n{e.strerror}")
            raise
        except RuntimeError as e:
            logging.critical(f"{e.__class__.__name__}\n{str(e)}")
            raise
        else:
            logging.info("SMTP-соединение установлено")
            self.smtp_server = smtp_server
            return

    def send_email(self, message: MessageYoutrack):
        """
        Sends the message to the mailbox.

        :param message: the message to deliver
        :type message: MessageYoutrack
        :return: None.
        """
        if self.smtp_server is None:
            logging.critical("SMTP-сервер не активен")
            raise OSError
        if message.email_message is None:
            logging.warning("Назначенных задач нет")
        else:
            try:
                with self.smtp_server:
                    self.smtp_server.sendmail(message.sender_email, message.recipient_email, message.email_message)
            except SMTPHeloError as e:
                logging.error(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
                return
            except SMTPSenderRefused as e:
                logging.error(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
                return
            except SMTPDataError as e:
                logging.error(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
                return
            except SMTPNotSupportedError as e:
                logging.error(f"{e.__class__.__name__}\n{e.strerror}")
                return
            except SMTPRecipientsRefused as e:
                logging.error(f"{e.__class__.__name__}\n{e.strerror}")
                return
            else:
                logging.info("Сообщение отправлено на почтовый ящик")
        # stop the SMTP server
        self.smtp_server.close()
        self.smtp_server = None
        logging.info("SMTP-сервер отключен")
        return


class User:
    """
    Specifies the main class to gather the full information.

    Params:
        _login --- the user login based on the environment variable "USER"/"USERNAME", str;\n
        _sender_email --- the address to send the message from, str;\n
        _recipient_email --- the address to receive the message, str;\n
        session --- the HTTP sessiion, Session;\n
        smtp_server --- the SMTP server, MailServer;\n

    Properties:
        _soon_deadline_responses --- the issues with the deadline, list[ResponseYoutrack];\n
        _no_deadline_responses --- the issues with no deadline, list[ResponseYoutrack];\n
        _message_text --- the message content, str;\n

    Functions:
        send_mail() --- deliver the email message;\n
    """

    _message_attributes: MessageAttributes = MessageAttributes(
        "tarasov-a",
        "Срочные задачи YouTrack, Исполнители_Doc/Doc_ST/Archive/Archive_ST",
        "text/plain; charset=UTF-8; format=flowed",
        "8bit",
        "ru",
        "1.0"
    )

    def __init__(self):
        if sys.platform == "Win32":
            self._login: str = os.environ["USER"]
        else:
            self._login: str = os.environ["USERNAME"]
        self._sender_email: str = f"tarasov-a@protei.ru"
        self._recipient_email: str = f"{self._login}@protei.ru"
        self.session: Session = Session()
        self.smtp_server: MailServer = MailServer()
        self.smtp_server.server()

    def __repr__(self):
        return f"<{self.__class__.__name__}(login={self._login})>"

    def __str__(self):
        return f"Пользователь: {self._login}, {self._recipient_email}"

    @property
    def _soon_deadline_responses(self) -> Optional[list[ResponseYoutrack]]:
        """
        Gets the issues with the deadline.

        :return: the Youtrack responses.
        :rtype: list[ResponseYoutrack] or None
        """
        _params_soon_deadline: tuple[tuple[str, str], ...] = RequestParamsWeekDeadline(self._login).params
        _request_soon_deadline: RequestYT = RequestYT(self.session, _params_soon_deadline)
        return _request_soon_deadline.parse_response()

    @property
    def _no_deadline_responses(self) -> Optional[list[ResponseYoutrack]]:
        """
        Gets the issues with no deadline.

        :return: the Youtrack responses.
        :rtype: list[ResponseYoutrack] or None
        """
        _params_no_deadline: tuple[tuple[str, str], ...] = RequestParamsNoDeadline(self._login).params
        _request_no_deadline: RequestYT = RequestYT(self.session, _params_no_deadline)
        return _request_no_deadline.parse_response()

    @property
    def _message_text(self) -> Optional[str]:
        """
        Specifies the message content.

        :return: the text of the message.
        :rtype: str or None
        """
        return MessageConverter(
            self._login,
            soon_deadline_responses=self._soon_deadline_responses,
            no_deadline_responses=self._no_deadline_responses).message_text()

    def send_mail(self):
        """Delivers the email message."""
        _message_youtrack: MessageYoutrack = MessageYoutrack(
            self._sender_email, self._recipient_email, self._message_text, self._message_attributes)
        self.smtp_server.send_email(_message_youtrack)


def main():
    logging.info("\n========================================")
    logging.info("Запуск программы:")
    _user: User = User()
    _user.send_mail()


if __name__ == "__main__":
    log_folder: Path = Path("./logs")
    log_folder.mkdir(parents=True, exist_ok=True)

    fmt: str = "{asctime}::{levelname} | {filename}::{name}::{lineno} | {msg}"
    datefmt: str = "%Y-%m-%d %H:%M:%S"
    style: Literal["%", "{", "$"] = "{"
    formatter: Formatter = Formatter(fmt, datefmt, style)

    stream: TextIO = sys.stdout
    stream_handler: StreamHandler = StreamHandler(stream)
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(formatter)

    filename: str = f"./logs/{__name__}_debug.log"
    mode: str = "w"
    max_bytes: int = 2 ** 20
    backup_count: int = 5
    encoding: str = "utf-8"
    file_handler = FileHandler(filename, mode, encoding)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)

    logger: Logger = logging.getLogger("root")
    logger.setLevel(logging.DEBUG)
    logger.addHandler(stream_handler)
    logger.addHandler(file_handler)

    main()
    user_input = input("Нажмите любую клавишу, чтобы закрыть окно ...")

    logging.shutdown()
    rmtree("./logs")
