import logging
import sys
from logging import Logger, Formatter, StreamHandler, FileHandler
from pathlib import Path
from shutil import rmtree
from typing import Literal, TextIO

from personal_script.user_processing import User


def main():
    logging.info("\n========================================")
    logging.info("Запуск программы:")
    _user: User = User()
    _user.send_mail()


if __name__ == "__main__":
    log_folder: Path = Path("./logs")
    log_folder.mkdir(parents=True, exist_ok=True)

    fmt: str = "{asctime}::{levelname} | {filename}::{name}::{lineno} | {msg}"
    datefmt: str = "%Y-%m-%d %H:%M:%S"
    style: Literal["%", "{", "$"] = "{"
    formatter: Formatter = Formatter(fmt, datefmt, style)

    stream: TextIO = sys.stdout
    stream_handler: StreamHandler = StreamHandler(stream)
    stream_handler.setLevel(logging.INFO)
    stream_handler.setFormatter(formatter)

    filename: str = f"./logs/{__name__}_debug.log"
    mode: str = "w"
    max_bytes: int = 2 ** 20
    backup_count: int = 5
    encoding: str = "utf-8"
    file_handler = FileHandler(filename, mode, encoding)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)

    logger: Logger = logging.getLogger("root")
    logger.setLevel(logging.DEBUG)
    logger.addHandler(stream_handler)
    logger.addHandler(file_handler)

    main()
    user_input = input("Нажмите любую клавишу, чтобы закрыть окно ...")
    logging.shutdown()
    rmtree("./logs")
