import logging
import os
import sys
from logging import Logger
from typing import Optional

from requests import Session

from personal_script.mail_server import MailServer
from personal_script.message_processing import MessageAttributes, MessageConverter, MessageYoutrack
from personal_script.youtrack_request import RequestParamsWeekDeadline, RequestYT, RequestParamsNoDeadline
from personal_script.youtrack_response import ResponseYoutrack

logger: Logger = logging.getLogger(f"rooot.{__name__}")

__all__ = ["User"]


class User:
    """
    Specifies the main class to gather the full information.

    Params:
        _login --- the user login based on the environment variable "USER"/"USERNAME", str;\n
        _sender_email --- the address to send the message from, str;\n
        _recipient_email --- the address to receive the message, str;\n
        session --- the HTTP sessiion, Session;\n
        smtp_server --- the SMTP server, MailServer;\n

    Properties:
        _soon_deadline_responses --- the issues with the deadline, list[ResponseYoutrack];\n
        _no_deadline_responses --- the issues with no deadline, list[ResponseYoutrack];\n
        _message_text --- the message content, str;\n

    Functions:
        send_mail() --- deliver the email message;\n
    """

    _message_attributes: MessageAttributes = MessageAttributes(
        "tarasov-a",
        "Срочные задачи YouTrack, Исполнители_Doc/Doc_ST/Archive/Archive_ST",
        "text/plain; charset=UTF-8; format=flowed",
        "8bit",
        "ru",
        "1.0"
    )

    def __init__(self):
        if sys.platform == "Win32":
            self._login: str = os.environ["USER"]
        else:
            self._login: str = os.environ["USERNAME"]
        self._sender_email: str = f"tarasov-a@protei.ru"
        self._recipient_email: str = f"{self._login}@protei.ru"
        self.session: Session = Session()
        self.smtp_server: MailServer = MailServer()
        self.smtp_server.server()

    def __repr__(self):
        return f"<{self.__class__.__name__}(login={self._login})>"

    def __str__(self):
        return f"Пользователь: {self._login}, {self._recipient_email}"

    @property
    def _soon_deadline_responses(self) -> Optional[list[ResponseYoutrack]]:
        """
        Gets the issues with the deadline.

        :return: the Youtrack responses.
        :rtype: list[ResponseYoutrack] or None
        """
        _params_soon_deadline: tuple[tuple[str, str], ...] = RequestParamsWeekDeadline(self._login).params
        _request_soon_deadline: RequestYT = RequestYT(self.session, _params_soon_deadline)
        return _request_soon_deadline.parse_response()

    @property
    def _no_deadline_responses(self) -> Optional[list[ResponseYoutrack]]:
        """
        Gets the issues with no deadline.

        :return: the Youtrack responses.
        :rtype: list[ResponseYoutrack] or None
        """
        _params_no_deadline: tuple[tuple[str, str], ...] = RequestParamsNoDeadline(self._login).params
        _request_no_deadline: RequestYT = RequestYT(self.session, _params_no_deadline)
        return _request_no_deadline.parse_response()

    @property
    def _message_text(self) -> Optional[str]:
        """
        Specifies the message content.

        :return: the text of the message.
        :rtype: str or None
        """
        return MessageConverter(
            self._login,
            soon_deadline_responses=self._soon_deadline_responses,
            no_deadline_responses=self._no_deadline_responses).message_text()

    def send_mail(self):
        """Delivers the email message."""
        _message_youtrack: MessageYoutrack = MessageYoutrack(
            self._sender_email, self._recipient_email, self._message_text, self._message_attributes)
        self.smtp_server.send_email(_message_youtrack)
