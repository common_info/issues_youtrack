import logging
from logging import Logger
from smtplib import SMTP, SMTPConnectError, SMTPServerDisconnected, SMTPNotSupportedError, SMTPAuthenticationError, \
    SMTPResponseException, SMTPException, SMTPHeloError, SMTPSenderRefused, SMTPDataError, SMTPRecipientsRefused
from ssl import SSLContext, create_default_context
from typing import Optional

from personal_script.ini_config_parser import IniConfigParser
from personal_script.message_processing import MessageYoutrack

logger: Logger = logging.getLogger(f"root.{__name__}")

__all__ = ["MailServer"]


class MailServer:
    """
    Specifies the mail SMTP server.

    Params:
        host --- the SMTP server DNS name, str;\n
        port --- the SMTP server port, int;\n
        user --- the login to authenticate in the server, str;\n
        password --- the password to authenticate in the server, str;\n

    Functions:
        server() --- run the SMTP server;\n
        send_email(message: MessageYoutrack) --- send the message to the mailbox.
    """
    ini_config_parser: IniConfigParser = IniConfigParser()
    _smtp_host: str = ini_config_parser.smtp_host
    _smtp_port: int = ini_config_parser.smtp_port
    _smtp_user: str = ini_config_parser.smtp_user
    _smtp_password: str = ini_config_parser.smtp_password

    def __init__(self, host: str = None, port: int = None, user: str = None, password: str = None):
        if host is None:
            host = self._smtp_host
        if port is None:
            port = self._smtp_port
        if user is None:
            user = self._smtp_user
        if password is None:
            password = self._smtp_password

        self.host: str = host
        self.port: int = port
        self.user: str = user
        self.password: str = password
        self.smtp_server: Optional[SMTP] = None

    def __repr__(self):
        return f"<{self.__class__.__name__}(host={self.host}, port={self.port}, user={self.user}, " \
               f"password={self.password})>"

    def __str__(self):
        return f"Mail server: {self.host}:{self.port}, SMTP server active: {self.smtp_server is None}"

    def server(self):
        """Runs the SMTP server to send emails. Further the server is used as a content manager."""
        context: SSLContext = create_default_context()
        smtp_server: SMTP = SMTP(self.host, self.port)
        try:
            logging.info(f"Попытка подключения к SMTP-серверу: {self.host}:{self.port}")
            smtp_server.connect(self.host, self.port)
            # set the connection
            smtp_server.ehlo()
            smtp_server.starttls(context=context)
            smtp_server.ehlo()
            # authenticate
            smtp_server.login(self.user, self.password)
        except SMTPConnectError as e:
            logging.critical(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
            raise
        except SMTPServerDisconnected as e:
            logging.critical(f"{e.__class__.__name__}\nFile: {e.filename}\n{e.strerror}")
            raise
        except SMTPNotSupportedError as e:
            logging.critical(f"{e.__class__.__name__}\nFile: {e.filename}\n{e.strerror}")
            raise
        except SMTPAuthenticationError as e:
            logging.critical(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
            raise
        except SMTPResponseException as e:
            logging.critical(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
            raise
        except SMTPException as e:
            logging.critical(f"{e.__class__.__name__}\nFile: {e.filename}\n{e.strerror}")
            raise
        except ConnectionRefusedError as e:
            logging.critical(f"{e.__class__.__name__}\nFile: {e.filename}\n{e.strerror}")
            raise
        except RuntimeError as e:
            logging.critical(f"{e.__class__.__name__}\n{str(e)}")
            raise
        else:
            logging.info("SMTP-соединение установлено")
            self.smtp_server = smtp_server
            return

    def send_email(self, message: MessageYoutrack):
        """
        Sends the message to the mailbox.

        :param message: the message to deliver
        :type message: MessageYoutrack
        :return: None.
        """
        if self.smtp_server is None:
            logging.critical("SMTP-сервер не активен")
            raise OSError
        if message.email_message is None:
            logging.warning("Назначенных задач нет")
        else:
            try:
                with self.smtp_server:
                    self.smtp_server.sendmail(message.sender_email, message.recipient_email, message.email_message)
            except SMTPHeloError as e:
                logging.error(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
                return
            except SMTPSenderRefused as e:
                logging.error(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
                return
            except SMTPDataError as e:
                logging.error(f"{e.__class__.__name__}\nSMTP Code: {e.smtp_code}\nError: {e.smtp_error}\n{e.strerror}")
                return
            except SMTPNotSupportedError as e:
                logging.error(f"{e.__class__.__name__}\n{e.strerror}")
                return
            except SMTPRecipientsRefused as e:
                logging.error(f"{e.__class__.__name__}\n{e.strerror}")
                return
            else:
                logging.info("Сообщение отправлено на почтовый ящик")
        # stop the SMTP server
        self.smtp_server.close()
        self.smtp_server = None
        logging.info("SMTP-сервер отключен")
        return
