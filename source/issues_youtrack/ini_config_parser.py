import logging
from configparser import ConfigParser, SectionProxy
from functools import cached_property
from logging import Logger
from pathlib import Path
from typing import Optional

logger: Logger = logging.getLogger(f"root.{__name__}")

__all__ = ["IniConfigParser"]


class IniConfigParser:
    """
    Specifies the *.INI configuration file parser.

    Params:
        _path --- the _path to the configuration file, str, default: "./config.ini";\n
        parser --- the config parser instance, ConfigParser;\n

    Properties:
        auth_token --- the authentication token, str;\n
        smtp_host --- the SMTP server host, str;\n
        smtp_port --- the SMTP server port, int;\n
        smtp_user --- the login to authenticate SMTP server, str;\n
        smtp_password --- the password to authenticate SMTP server, str;\n

    Functions:
        _get_section(section: str) --- get the section of the file, SectionProxy;\n
        _get_param(section: str, option: str) --- get the parameter of the file, str;\n
        _convert_to_int(section: str, option: str) --- get the integer parameter of the file, int;\n
        _get_youtrack_param(option: str) --- get the [Youtrack] section parameter, str;\n
        _get_smtp_param(option: str) --- get the [SMTP] section parameter, str;\n
    """
    _instance = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, path: str = None):
        if path is None:
            path = Path(__file__).resolve().with_name("config.ini")
        self._path: str = path
        self._parser = ConfigParser()
        self._parser.read(self._path)

    def __repr__(self):
        return f"<{self.__class__.__name__}(_path={self._path})>"

    def __str__(self):
        return f"{self.__class__.__name__}:\nauth_token = {self.auth_token}\nSMTP host = {self.smtp_host}\n" \
               f"SMTP port = {self.smtp_port}\nSMTP user = {self.smtp_user}"

    def _get_section(self, section: str) -> Optional[SectionProxy]:
        """
        Reads the section of the configuration file if exists.

        :param str section: the section name
        :return: the file section if exists.
        :rtype: SectionProxy or None
        :raise ValueError if the section does not exist.
        """
        if self._parser.has_section(section):
            return self._parser[section]
        else:
            logging.error(f"Секция {section} не найдена")
            raise ValueError

    def _get_param(self, section: str, option: str) -> Optional[str]:
        """
        Reads the parameter of the configuration file if exists.

        :param str section: the section name
        :param str option: the parameter name
        :return: the parameter value if exists.
        :rtype: str or None
        :raise ValueError if the section or the parameter does not exist.
        """
        if self._parser.has_option(section, option):
            return self._parser[section][option]
        else:
            logging.error(f"Секция {section} с параметром {option} не найдена")
            raise ValueError

    def _convert_to_int(self, section: str, option: str) -> Optional[int]:
        """
        Reads the parameter of the configuration file as an integer if exists.

        :param str section: the section name
        :param str option: the parameter name
        :return: the parameter value if exists.
        :rtype: int or None
        :raise ValueError if the section or the parameter does not exist.
        """
        _string_value: Optional[str] = self._get_param(section, option)
        if _string_value:
            try:
                _int_value: int = int(_string_value)
            except ValueError as e:
                logging.error(
                    f"{e.__class__.__name__}, значение {_string_value} не может быть представлено в виде числа")
                return
            else:
                return _int_value

    def _get_youtrack_param(self, option: str) -> Optional[str]:
        """
        Get the parameter of the [Youtrack] section.

        :param str option: the parameter name
        :return: the parameter value.
        :rtype: str or None
        """
        section = self._get_section("Youtrack")
        return self._get_param(section.name, option)

    def _get_smtp_param(self, option: str) -> Optional[str]:
        """
        Get the parameter of the [SMTP] section.

        :param str option: the parameter name
        :return: the parameter value.
        :rtype: str or None
        """
        section = self._get_section("SMTP")
        return self._get_param(section.name, option)

    @cached_property
    def auth_token(self) -> Optional[str]:
        """
        Gets the authentication token value.

        :return: the value.
        :rtype: str or None
        """
        return self._get_youtrack_param("AUTH_TOKEN")

    @cached_property
    def smtp_host(self) -> Optional[str]:
        """
        Gets the SMTP host value.

        :return: the value.
        :rtype: str or None
        """
        return self._get_smtp_param("SMTP_HOST")

    @cached_property
    def smtp_port(self) -> Optional[int]:
        """
        Gets the SMTP port value.

        :return: the value.
        :rtype: int or None
        """
        return self._convert_to_int("SMTP", "SMTP_PORT")

    @cached_property
    def smtp_user(self) -> Optional[str]:
        """
        Gets the SMTP user login to autheticate.

        :return: the login.
        :rtype: str or None
        """
        return self._get_smtp_param("SMTP_USER")

    @cached_property
    def smtp_password(self) -> Optional[str]:
        """
        Gets the SMTP password to autheticate.

        :return: the password.
        :rtype: str or None
        """
        return self._get_smtp_param("SMTP_PASSWORD")
