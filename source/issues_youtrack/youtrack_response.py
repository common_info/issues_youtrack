from datetime import date, timedelta
from typing import NamedTuple, Optional


class ResponseYoutrack(NamedTuple):
    """
    Contains the parsed response instance.

    Params:
        issue --- the issue name (idReadable), str;\n
        state --- the issue state (customFields.State), str;\n
        summary --- the short issue description (summary), str;\n
        deadline --- the last day to complete the work (customFields.Дедлайн), str (ISO format);\n
        priority --- the issue importance (customFields.Priority), str (Low/Basic/Critical).\n

    Properties:
        hyperlink --- the YouTrack link to the issue, str.

    Functions:
        convert_for_json() --- format the instance to write to the JSON file, dict[str, str].
    """

    issue: str
    state: str
    summary: str
    deadline: str
    priority: str

    def __repr__(self):
        return f"<ResponseYoutrack(issue={self.issue}, state={self.state}, summary={self.summary}," \
               f"deadline={self.deadline}, priority={self.priority})>"

    def __str__(self):
        if self.deadline:
            deadline_string = f"{self.deadline}, до дедлайна: {self.days_before} дней"
        else:
            deadline_string = "не задан"
        return f"Задача YouTrack: {self.issue}, статус: {self.state}, дедлайн: {deadline_string}." \
               f"\nПерейти к задаче: {self.hyperlink}"

    def __hash__(self):
        return hash(self.issue)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.issue == other.issue
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.issue != other.issue
        else:
            return NotImplemented

    def __lt__(self, other):
        if isinstance(other, self.__class__) and self.issue != other.issue:
            if self._priority_conversion[self.priority] < other._priority_conversion[other.priority]:
                return True
            elif self._priority_conversion[self.priority] > other._priority_conversion[other.priority]:
                return False
            else:
                if self._deadline_date < other._deadline_date:
                    return True
                elif self._deadline_date > other._deadline_date:
                    return False
                else:
                    return self._issue_number < other._issue_number
        else:
            return NotImplemented

    def __gt__(self, other):
        if isinstance(other, self.__class__) and self.issue != other.issue:
            if self.priority_int > other.priority_int:
                return True
            elif self.priority_int < other.priority_int:
                return False
            else:
                if self._deadline_date > other._deadline_date:
                    return True
                elif self._deadline_date < other._deadline_date:
                    return False
                else:
                    return self._issue_number > other._issue_number
        else:
            return NotImplemented

    @property
    def _priority_conversion(self) -> dict[str, int]:
        """
        Mapping of the priorities and integers.

        :return: the dictionary.
        :rtype: dict[str, int]
        """
        return {
            "Critical": 30,
            "Important": 20,
            "Basic": 10,
            "Low": 0
        }

    @property
    def hyperlink(self) -> str:
        """
        Specifies the hyperlink to the issue in the Youtrack.

        :return: the active hyperlink.
        :rtype: str
        """
        return f"https://youtrack.protei.ru/issue/{self.issue}"

    @property
    def days_before(self) -> Optional[int]:
        """
        Specifies the number of remaining days up to the deadline if the deadline exists.

        :return: the number of days or None.
        :rtype: int or None
        """
        if self.deadline:
            _deadline_date: date = date.fromisoformat(self.deadline)
            _today: date = date.today()
            _delta: timedelta = _deadline_date - _today
            return _delta.days
        return None

    @property
    def _deadline_date(self) -> date:
        """
        Converts the deadline to the date format. If the deadline is missing, returns the date in the farthest future.\n
        Used for sorting issues.

        :return: the deadline date.
        :rtype: date
        """
        if self.deadline:
            return date.fromisoformat(self.deadline)
        else:
            return date(9999, 1, 1)

    @property
    def _issue_number(self) -> int:
        """
        Gets the issue number from the full issue id value, <project>-<issue_number>.\n
        Used for sorting issues.

        :return: the issue number.
        :rtype: int
        """
        return int(self.issue.split("-")[1])

    @property
    def priority_int(self) -> int:
        """
        Maps the issue priority to the integer following the conversion table.\n
        Used for sorting issues.

        :return: the associated priority integer value.
        :rtype: int
        """
        return self._priority_conversion[self.priority]
